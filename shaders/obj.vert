#version 330

in vec3 inPosition;
in vec3 inNormal;

out vec3 normal;

uniform mat4 uModel;
uniform mat4 uProj;
uniform mat4 uView;

void main() {
    normal = inNormal;
    gl_Position = uProj * uView * uModel * vec4(inPosition, 1.0);
}
