#version 330

in vec2 texCoord;
uniform sampler2D sceneTexture;
out vec4 outColor;

float WIDTH = 600.f;
float HEIGHT = 400.f;

void main() {
    vec4 baseColor = texture(sceneTexture, texCoord);
    float grayColor = baseColor.r * 0.33 + baseColor.g * 0.33 + baseColor.b * 0.33;

    if(gl_FragCoord.x > 300)
        outColor = baseColor;
    else
        outColor = vec4(grayColor, grayColor, grayColor, 1.0);

    if(300 < gl_FragCoord.x && gl_FragCoord.x < 301)
        outColor = vec4(1., 0., 0., 1.);

    if(200 < gl_FragCoord.y && gl_FragCoord.y < 201)
        outColor = vec4(1., 0., 0., 1.);

//    if( WIDTH / 2 - 50 < gl_FragCoord.x && gl_FragCoord.x < WIDTH / 2 + 50
//    && HEIGHT / 2 - 50 < gl_FragCoord.y && gl_FragCoord.y < HEIGHT / 2 + 50)
//        discard;


}
