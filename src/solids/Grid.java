package solids;

public class Grid extends Solid {

    /**
     * Gl_TRIANGLES
     * @param m - počet vtcholů v řádku
     * @param n - počet vrcholů ve sloupci
     */
    public Grid(final int m, final int n) {
        // vb
        float[] vb = new float[2 * m * n];
        // ib
        int[] ib = new int[3 * 2 * (m - 1) * (n - 1)];

        // generování vb
        int index = 0;
        // rozsah <0; 1>
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                vb[index++] = j / (float)(m - 1);
                vb[index++] = i / (float)(n - 1);
            }
        }

        index = 0;
        for (int i = 0; i < n - 1; i++) {
            int offset = i * m;
            for (int j = 0; j < m - 1; j++) {
                // j = 0, m = 4
                ib[index++] = j + offset; // 0
                ib[index++] = j + m + offset; // 4
                ib[index++] = j + 1 + offset; // 1

                ib[index++] = j + 1 + offset; // 1
                ib[index++] = j + m + offset; // 4
                ib[index++] = j + m + 1 + offset; // 5
            }
        }

        lwjglutils.OGLBuffers.Attrib[] attributes = {
                new lwjglutils.OGLBuffers.Attrib("inPosition", 2)
        };

        buffers = new lwjglutils.OGLBuffers(vb, attributes, ib);
    }
}
