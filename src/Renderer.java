import lwjglutils.OGLModelOBJ;
import lwjglutils.OGLRenderTarget;
import lwjglutils.OGLTexture2D;
import lwjglutils.ShaderUtils;
import org.lwjgl.BufferUtils;
import solids.*;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import transforms.*;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;


/**
* 
* @author PGRF FIM UHK
* @version 2.0
* @since 2019-09-02
*/
public class Renderer extends AbstractRenderer{

	private int shaderProgamTriangle, shaderProgamAxis, shaderProgamGrid,
			shaderProgramLight, shaderProgramPost, shaderProgramObj;
	private Triangle triangle;
	private Axis axisX, axisY, axisZ;
	private Grid grid, light, postQuad;
	private Camera camera;
	private Mat4PerspRH proj;
	private float time;
	private Vec3D lightPosition;

	private	OGLTexture2D texture;

	private OGLRenderTarget renderTarget;
	private OGLTexture2D.Viewer viewer;

	// mouse
	double ox, oy;
	boolean mouseButton1 = false;

	// Obj
	private OGLModelOBJ objModel;


    @Override
    public void init() {
		triangle = new Triangle();
		shaderProgamTriangle = lwjglutils.ShaderUtils.loadProgram("/triangle");

		axisX = new Axis(1.f, 0.f, 0.f, new Col(1.f, 0.f, 0.f));
		axisY = new Axis(0.f, 1.f, 0.f, new Col(0.f, 1.f, 0.f));
		axisZ = new Axis(0.f, 0.f, 1.f, new Col(0.f, 0.f, 1.f));
		shaderProgamAxis = lwjglutils.ShaderUtils.loadProgram("/axis");

		grid = new Grid(30, 30);
		shaderProgamGrid = lwjglutils.ShaderUtils.loadProgram("/morph2");

		// cam, proj
		camera = new Camera()
				.withPosition(new Vec3D(-2.5f, -2.5f, 3.f))
				.withAzimuth(Math.toRadians(45))
				.withZenith(Math.toRadians(-45))
				/*
				.withPosition(new Vec3D(0.f, 0.f, 3.f))
				.withAzimuth(Math.toRadians(0))
				.withZenith(Math.toRadians(-90))
				 */
				.withFirstPerson(true);
		proj = new Mat4PerspRH(Math.PI / 4, height / (float)width, 0.1f, 100.f);

		// texture
		try {
			texture = new OGLTexture2D("textures/globe.jpg");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// světlo
		light = new Grid(2, 2);
		shaderProgramLight = ShaderUtils.loadProgram("/light");
		lightPosition = new Vec3D(0.f, 0.f, 1000.1f);

		// postprocessing
		renderTarget = new OGLRenderTarget(width, height);
		postQuad = new Grid(2, 2);
		shaderProgramPost = ShaderUtils.loadProgram("/post");
		viewer = new OGLTexture2D.Viewer();

		// obj
		objModel = new OGLModelOBJ("/obj/ElephantBody.obj");
		shaderProgramObj = lwjglutils.ShaderUtils.loadProgram("/obj");

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    @Override
    public void display() {
		drawScene();
		postProcessing();

		viewer.view(renderTarget.getColorTexture(), -1, -1, 1, height / (double) width);
		viewer.view(texture, -1, 0, 1, height / (double) width);
	}

	public void drawScene() {
		renderTarget.bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

		drawObj();
		//drawGrid();
		drawAxis();
	}

	public void postProcessing() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(shaderProgramPost);

		renderTarget.getColorTexture().bind(shaderProgramPost, "sceneTexture");
		postQuad.getBuffers().draw(GL_TRIANGLES, shaderProgramPost);
	}

	private void drawTriangle() {
		glUseProgram(shaderProgamTriangle);
		setGlobalUniforms(shaderProgamTriangle);
		triangle.getBuffers().draw(GL_TRIANGLES, shaderProgamTriangle);
	}

	private void drawAxis() {
		glUseProgram(shaderProgamAxis);
		setGlobalUniforms(shaderProgamAxis);
		axisX.getBuffers().draw(GL_LINES, shaderProgamAxis);
		axisY.getBuffers().draw(GL_LINES, shaderProgamAxis);
		axisZ.getBuffers().draw(GL_LINES, shaderProgamAxis);
	}

	private void drawGrid() {
		glUseProgram(shaderProgamGrid);
		setGlobalUniforms(shaderProgamGrid);

		int locUTime = glGetUniformLocation(shaderProgamGrid, "uTime");
		glUniform1f(locUTime, time);
		texture.bind(shaderProgamGrid, "textureBricks");
		int locULightPos = glGetUniformLocation(shaderProgamGrid, "u_LightPos");
		glUniform3f(locULightPos, (float)lightPosition.getX(), (float)lightPosition.getY(), (float)lightPosition.getZ());

		grid.getBuffers().draw(GL_TRIANGLES, shaderProgamGrid);
	}

	private void drawObj() {
		glUseProgram(shaderProgramObj);
		setGlobalUniforms(shaderProgramObj);

		int locUModel = glGetUniformLocation(shaderProgramObj, "uModel");
		glUniformMatrix4fv(locUModel, false, new Mat4Scale(0.01, 0.01, 0.01).floatArray());


		objModel.getBuffers().draw(GL_TRIANGLES, shaderProgramObj);
	}

	private void drawLight() {
		glUseProgram(shaderProgramLight);
		setGlobalUniforms(shaderProgramLight);
		light.getBuffers().draw(GL_TRIANGLES, shaderProgramLight);
	}

	private void setGlobalUniforms(int shaderProgram) {
		int locUView = glGetUniformLocation(shaderProgram, "uView");
		int locUProj = glGetUniformLocation(shaderProgram, "uProj");

		glUniformMatrix4fv(locUView, false, camera.getViewMatrix().floatArray());
		glUniformMatrix4fv(locUProj, false, proj.floatArray());
	}

	private GLFWKeyCallback   keyCallback = new GLFWKeyCallback() {
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
		}
	};
    
    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
        @Override
        public void invoke(long window, int w, int h) {
        }
    };
    
    private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback () {
		@Override
		public void invoke(long window, int button, int action, int mods) {
			mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
				mouseButton1 = true;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				ox = xBuffer.get(0);
				oy = yBuffer.get(0);
			}

			if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
				mouseButton1 = false;
				DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(window, xBuffer, yBuffer);
				double x = xBuffer.get(0);
				double y = yBuffer.get(0);
				camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
						.addZenith((double) Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
			}
		}
		
	};
	
    private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
			if (mouseButton1) {
				camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
						.addZenith((double) Math.PI * (oy - y) / width);
				ox = x;
				oy = y;
			}
    	}
    };
    
    private GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
        @Override public void invoke (long window, double dx, double dy) {
        }
    };
 

	@Override
	public GLFWKeyCallback getKeyCallback() {
		return keyCallback;
	}

	@Override
	public GLFWWindowSizeCallback getWsCallback() {
		return wsCallback;
	}

	@Override
	public GLFWMouseButtonCallback getMouseCallback() {
		return mbCallback;
	}

	@Override
	public GLFWCursorPosCallback getCursorCallback() {
		return cpCallbacknew;
	}

	@Override
	public GLFWScrollCallback getScrollCallback() {
		return scrollCallback;
	}
}